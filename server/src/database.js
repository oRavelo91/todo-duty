const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/mean-duty', {
	useUnifiedTopology: true,
	useNewUrlParser: true,
	useFindAndModify: false
})
.then(db => console.log('Db is Connected'))
.catch(err => console.log(err));
const dutyController = {}

const Duty = require('../models/Duty');

dutyController.getDuties = async (req, res) => {
	const duties = await Duty.find();
	res.json(duties);
};
dutyController.createDuty = async (req, res) => {
	const newDuty = new Duty(req.body)
	await newDuty.save();
	res.send({ message: 'Duty created' });
}
dutyController.getDuty = async (req, res) => {
	const duty = await Duty.findById(req.params.id)
	res.send(duty);
}
dutyController.editDuty = async(req, res) => { 

	await Duty.findByIdAndUpdate(req.params.id, req.body)
	res.json({status: 'duty updated'});
}
dutyController.deleteDuty = async (req, res) => {
	await Duty.findByIdAndDelete(req.params.id);
	res.json({ status: 'Duty Deleted' })
}

module.exports = dutyController;
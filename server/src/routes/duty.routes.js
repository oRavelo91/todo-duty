const { Router } = require('express')

const router = Router();
const dutyController = require('../controllers/duty.controller');


router.get('/', dutyController.getDuties );

router.get('/:id', dutyController.getDuty );

router.post('/', dutyController.createDuty );

router.put('/:id', dutyController.editDuty );

router.delete('/:id', dutyController.deleteDuty );



module.exports = router;
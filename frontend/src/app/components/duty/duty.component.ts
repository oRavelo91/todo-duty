import { Component, OnInit } from '@angular/core';
import {DutyService } from '../../services/duty.service'
import {NgForm} from '@angular/forms'
import { Duties } from 'src/app/models/duty';
@Component({
  selector: 'app-duty',
  templateUrl: './duty.component.html',
  styleUrls: ['./duty.component.css']
})
export class DutyComponent implements OnInit {

  constructor(public dutyService: DutyService) { }

  ngOnInit(): void {
    this.getDuties();
  }
  getDuties(){

   this.dutyService.getDuties().subscribe(
     res => {this.dutyService.duties = res},
     error => console.log(error)

   )
  }

  addDuty(form: NgForm){
    if(form.value._id){
      this.dutyService.editDuty(form.value).subscribe(res => {
        this.getDuties()
      }, err => console.log(err))
    } else {


      this.dutyService.createDuty(form.value).subscribe(
        res => {
          this.getDuties();
          form.reset();
        },
        error => console.log(error)
        )
      }
  }

  deleteDuty(_id: string){
   const res =  confirm('Are you sure you want to delete it?')
   if (res){
     this.dutyService.deleteDuty(_id).subscribe(res => {
       this.getDuties();
     }, err => console.log(err))
   }
  }

  editDuty(duty: Duties){
    this.dutyService.selectedDuty = duty;
  }

  resetForm(form: NgForm){
    form.reset();
  }
}

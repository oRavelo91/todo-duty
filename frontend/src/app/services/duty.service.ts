import {  Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Duties } from '../models/duty';


@Injectable({
  providedIn: 'root'
})
export class DutyService {
URL_API = "http://localhost:3000/api/duties";
selectedDuty: Duties = {Name: '', _id: ''};
duties: Duties[] = [];
  constructor(private http: HttpClient) {
  }
  getDuties(){
    return this.http.get<Duties[]>(this.URL_API);
  }

  createDuty(duty: Duties){
    return this.http.post(this.URL_API, duty)
  }

  deleteDuty(_id: string){
    return this.http.delete(`${this.URL_API}/${_id}`)
  }

  editDuty(duty: Duties){
    return this.http.put(`${this.URL_API}/${duty._id}`, duty);
  }
}



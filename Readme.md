#MEAN STACK


## Server Folder
* controllers
	_where the CRUD methods are created_
* models
	_The Schema of the Duties_
* routes
	_The Routes of the api_
* app.js 
	_the run of the server_
* database.js
	_where you connect to mongodb with mongoose_
* index.js
	_where we define the port of the server_

## Frontend Folder
* duty 
	_folder where the duty components is created_
* model
	_where is declared the interface of Duty_
* services
	_all the interactions with the server are created here_
* app component
	_where the duty component is initialized_